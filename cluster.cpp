#include <iostream>
#include <exception>
#include <typeinfo>
#include <map>
#include <vector>
#include "helper/benchmark.hpp"
#include "helper/inout.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "helper/debug.hpp"
#include "cluster/optics.hpp"


cluster::MapD<string> read_scores(const string &score_file, vector<string> &db,
	map<string, string*> &mapping) {
	cluster::MapD<string> scores;
	dbgmsg("reading file " << score_file);
	vector<string> raw_score;
	inout::Inout::read_file(score_file, raw_score);
	db.reserve(raw_score.size());
	for (auto &line : raw_score) {
		stringstream ss(line);
		string conf;
		double score;
		ss>>conf>>score;
		db.push_back(conf);
		mapping[conf] = &db.back();
		scores[mapping[conf]] = score;
		//~ dbgmsg(" conf " << conf << " " << scores[mapping[conf]]);
	}
	//~ for (auto &s : db)
		//~ mapping[s] = &s;
	return scores;
}
cluster::PairwiseDistances<string> read_pairwise(const string &distance_file, 
	map<string, string*> &mapping) {

	cluster::PairwiseDistances<string> pairwise_distances;
	dbgmsg("reading file " << distance_file << " and hashing pairwise distances");
	vector<string> raw_distance;
	inout::Inout::read_file(distance_file, raw_distance);
	for (string &line : raw_distance) {
		stringstream ss(line);
		string conf1, conf2;
		double distance;
		ss>>conf1>>conf2>>distance;
		//~ if (!mapping.count(conf1)) throw Error("problem");
		//~ if (!mapping.count(conf2)) throw Error("problem");
		//~ pairwise_distances[conf1][conf2] = pairwise_distances[conf2][conf1] = distance;
		pairwise_distances[mapping[conf1]][mapping[conf2]] = distance;
		//~ dbgmsg(conf1 << " " << conf2 << " " << pairwise_distances[mapping[conf1]][mapping[conf2]]);
	}
	return pairwise_distances;
}
	
int main(int argc, char* argv[]) {
	try {
		dbgmsg("scores file = " << argv[1] << " pairwise file = " << argv[2]);
		vector<string> db;
		map<string, string*> mapping;
		cluster::MapD<string> scores = read_scores(argv[1], db, mapping);
		cluster::PairwiseDistances<string> pairwise_distances = read_pairwise(argv[2], mapping);
		cluster::Optics<string, std::greater<double>, std::less<double>> optics(pairwise_distances, scores, 2.0, 1);
		//~ cluster::Optics<string, std::greater<double>, std::greater<double>> optics(pairwise_distances, scores, 2.0, 10);
		auto result = optics.extract_dbscan(3.0);
		
		for (auto &kv : result.first) {
			cout << "CLUSTER\t" << kv.first << "\t" << *kv.second << endl;
		}
		for (auto &kv : result.second) {
			cout << "REPRESENTATIVE\t" << kv.first << "\t" << *kv.second << endl;
		}

	} catch (exception& e) {
		cerr << e.what() << endl;
	}
	return 0;
}
